export default interface IState {
    people: {
        name: string
        position: string
        email: string
    }[]
}